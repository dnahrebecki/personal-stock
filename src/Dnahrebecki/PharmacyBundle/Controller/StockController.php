<?php
/**
 * Created by PhpStorm.
 * User: dnahrebecki
 * Date: 12.07.15
 */

namespace Dnahrebecki\PharmacyBundle\Controller;

use Dnahrebecki\StockBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class StockController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function listAction()
    {
        $productRepository = $this->getDoctrine()->getRepository('DnahrebeckiStockBundle:Product');

        /** @var Product[] $products */
        $products = $productRepository->findAll();

        echo count($products);

        return ['products' => $products];
    }
}